let gulp = require('gulp');
let sass = require('gulp-sass');
let concat = require('gulp-concat');

gulp.task('scss', () => {
  return gulp.src([
    'gulp/assets/calendar.scss'
  ])
    .pipe(sass())
    .pipe(concat('style.css'))
    .pipe(gulp.dest('css'));
});

gulp.task('watch', ['scss'], () => {
  gulp.watch('gulp/assets/**/*.scss', ['scss']);
});

gulp.task('default', ['scss']);
